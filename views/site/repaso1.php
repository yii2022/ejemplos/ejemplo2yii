<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
?>

<?php
    foreach($fotos as $foto){
        echo Html::img("@web/imgs/{$foto}", [
            "alt"=>"imagen",
            "class"=>"imagen",
        ]);
    }
?>
